import React, { Component} from 'react'
import { Container, Form, Row, Button, Table } from 'react-bootstrap'
import {getCategory,deleteCategory,postCategory,editCategory} from '../redux/actions/article/articleActions'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import swal from 'sweetalert';
class Category extends Component {
    constructor(props){
        super(props);
        this.state={
            name: "",
            isEdit: false,
            btn: "Add Category",
            editID: "",
        }
    }
    componentDidMount(){
        this.props.getCategory()
        console.log(this.props.data)
    }
    componentWillUpdate(){
        this.props.getCategory()
        console.log(this.props.data)
    }
    handleDelete=(id)=>{
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this data!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
                this.props.deleteCategory(id);
                this.componentWillUpdate()
              swal("Data has been deleted!", {
                icon: "success",
              });
            } else {
              swal("Your data is safe!");
            }
          });
    }

    handleChange=(event)=> {
        this.setState({name: event.target.value});
    }
    handleEdit=(data)=>{
        this.setState({
            name:data.name,
            isEdit: true,
            btn: "Save",
            editID: data._id
        })
        console.log(data._id);
    }
    handleSubmit=(event)=>{
        console.log(this.state.name);
        console.log(this.props.data);
        if(this.state.isEdit){
            this.props.editCategory(this.state.editID,this.state.name)
            console.log(this.state.editID);
            this.setState({
                name: "",
                editID: "",
                isEdit: false,
                btn: "Add Category"
            })
            this.componentWillUpdate()
            swal({
                title: "Edited Successfully!",
                text: "Data has been edited!",
                icon: "success",
                button: "Aww yiss!",
            });
        }
        else{
            this.props.postCategory(this.state.name)
            this.setState({
                name: "",
            })
            this.componentWillUpdate()
            swal({
                title: "Added Succesfully!",
                text: "Category has been added!",
                icon: "success",
            });
        }
        event.preventDefault();
    }
    render() {
        var cateID = 1
        const data = this.props.data.map((data)=>{
            return <tbody key={data._id}>
                        <tr>
                            <td>{cateID++}</td>
                            <td>{data.name}</td>
                            <td>
                                <Button variant="info" style={{borderBottom:"5px solid mediumspringgreen",marginRight: "15px"}} className="mr-1" onClick={()=>this.handleEdit(data)} >Edit</Button>
                                <Button variant="danger" style={{borderBottom:"5px solid yellow"}} onClick={(id)=> this.handleDelete(data._id)}>Delete</Button>
                            </td>
                        </tr>
                    </tbody>
        })
        return (
            <Container>
                <h3>Category</h3>
                <Row>
                    <Form inline>
                        <Form.Group>
                            <Form.Control style={{marginLeft: "15px",marginRight: "10px"}} value={this.state.name} onChange={this.handleChange} type="text" placeholder="Input Category Name"/>
                            <Button type="button" variant="dark" onClick={this.handleSubmit}>{this.state.btn}</Button>
                        </Form.Group>
                    </Form>
                </Row>
                <Row>
                <Table striped bordered hover style={{marginLeft: "15px", marginTop: "20px", textAlign: "center"}}>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    {data}
                    </Table>
                </Row>
            </Container>
        )
    }
}
const mapStateToProps = (state)=>{
    return {data:state.articleReducer.cate}
    
}
const mapDispatchToProps = (dispatch)=>{
    return bindActionCreators({
        getCategory,
        deleteCategory,
        postCategory,
        editCategory
    },dispatch)
}
export default connect(mapStateToProps,mapDispatchToProps)(Category)
