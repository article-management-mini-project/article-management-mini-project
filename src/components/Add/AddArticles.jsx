import React, { Component } from "react";
import { Form, Button } from "react-bootstrap";
import Axios from "axios";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {postPosts,addArticle} from '../../redux/actions/article/articleActions'
import { Link } from "react-router-dom";
class AddArticles extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        title: "",
        description: "",
        category: "",
        img: "",
      },
      category: [],
      imgFile: "",
      imgLink: "",
      DefaultImag: ""
    };
  }

  onImageChange = (e) => {
    this.setState({
      imgFile: e.target.files[0],
      imgLink: URL.createObjectURL(e.target.files[0]),
    });
  };

  handleTitle = (event) => {
    this.setState({
      data: {
        title: event.target.value,
      },
    });
  };

  handleDes = (event) => {
    this.setState({
      data: {
        ...this.state.data,
        description: event.target.value,
      },
    });
  };


  newArticle = () => {
    if (this.state.imgFile) {
      let file = new FormData();
      file.append("image", this.state.imgFile);
      postPosts(file, (res) => {
        let article = {
          title: this.state.data.title,
          description: this.state.data.description,
          image: res,
        };
        addArticle(article, (msg) => {
          alert(msg);
          this.props.history.push("/");
        });
      });
    } else {
      let article = {
        title: this.state.data.title,
        description: this.state.data.description,
      };
      addArticle(article, (msg) => {
        alert(msg);
        this.props.history.push("/");
      });
    }
  };


  handleRadio = (event) => {
    this.setState({
      data: {
        ...this.state.data,
        category: event.target.value,
      },
    });
    console.log(event.target.value);
  };
  componentWillMount() {
    Axios.get("http://110.74.194.125:3535/api/category").then((res) => {
      this.setState({
        category: res.data.data,
      });
    });
  }
  render() {
    console.log("imgasdfad" + this.state.imgLink);
    let categories = this.state.category.map((item, index) => (
      <div className="custom-control custom-radio d-inline mx-2" key={index}>
        <input
          type="radio"
          id={item._id}
          name="category"
          className="custom-control-input"
          value={item._id}
          onChange={this.handleRadio}
        />
        <label className="custom-control-label">{item.name}</label>
      </div>
    ));

    return (
      <div className="container my-4">
        <div className="row">
          <h1 className="mx-3">Add Article</h1>
        </div>
        <div className="row">
          <div className="col">
            <Form>
              <Form.Group controlId="formBasicTitle">
                <Form.Label>Title</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Input Title"
                  name="title"
                  onChange={this.handleTitle}
                />
              </Form.Group>
              <Form.Group className="row">
                <Form.Label as="legend" column={2}>
                  Category : {categories}
                </Form.Label>
              </Form.Group>

              <Form.Group controlId="formBasicDescription">
                <Form.Label>Description</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Input Description"
                  name="description"
                  onChange={this.handleDes}
                />
              </Form.Group>

              <Form.Group controlId="formBasicThumbnail">
                <Form.Label>Thumbnail</Form.Label>
                <div className="mb-3">
                  <Form.File id="formcheck-api-custom" custom>
                    <Form.File.Input
                      isValid
                      ref="add"
                      onChange={this.onImageChange.bind(this)}
                    />
                    <Form.File.Label
                      data-browse="Browse"
                      id="img"
                      src={this.state.imgLink}
                    >
                      Custom File Input
                    </Form.File.Label>
                  </Form.File>
                </div>
              </Form.Group>

              <Link to="/">
                <Button
                  variant="primary"
                  type="submit"
                   onClick={this.newArticle.bind(this)}
                >
                  Submit
                </Button>
              </Link>
            </Form>
          </div>
          <div className="col">
            <img
              src={this.state.imgLink==""? this.state.DefaultImage:this.state.imgLink}
              alt="Avatar"
              style={{ width: "70%", height: "75%", borderRadius: 10 }}
            ></img>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    data: state.data,
    img: state.img,
  };
};
const mapDispatchToProps = (dispatch) => {
  const all = bindActionCreators(
    {
      postPosts,
      addArticle
    },
    dispatch
  );
  return all;
};

export default connect(mapStateToProps, mapDispatchToProps)(AddArticles);