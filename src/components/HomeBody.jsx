import React, { Component } from 'react'
import { Col,Button,Row,Form
        ,FormControl} from 'react-bootstrap'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {getArticles,getMoreArticles,getArticlesById,updateArticlesById,getCategory,searchArticle,deleteArticle,addArticle} from '../redux/actions/article/articleActions'
import moment from 'moment';
import {Link} from 'react-router-dom';
import InfiniteScroll from 'react-infinite-scroll-component';
import Language from '../Language/Language' 
import {BoxLoading} from 'react-loadingg'
import swal from 'sweetalert';
const defaultImg = "https://increasify.com.au/wp-content/uploads/2016/08/default-image.png"
 class HomeBody extends Component {
    constructor(props){
        super(props);
        this.state={
            pageNumber:1,
            hasNext:true
        }
        // this.getMoreArticles.bind(this)
    }
    componentDidMount(){
        this.props.getArticles()
        this.props.getCategory()
    }
    getMoreArticles = () =>{
        this.setState({
            pageNumber:this.state.pageNumber+1
        })
        this.props.getMoreArticles(this.state.pageNumber,(length)=>{
            if(length===0)
                this.setState({
                    ...this.state,
                    hasNext:false
                })
        })
    }
    handleDelete=(id)=>{
        swal({
            title: "Are you sure want to delete this data?",
            text: "Once deleted, you will not be able to recover this data!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
                this.props.deleteArticle(id)
                this.componentDidMount()
              swal("Data has been deleted!", {
                icon: "success",
              });
            } else {
              swal("Your Data is safe!");
            }
          });
    }
    handleSearch=(event)=>{
        this.props.searchArticle(event.target.value)
        console.log(event.target.value)
    }
    handleAdd=()=>{
        
    }
    render() {
        let data = this.props.data.map((data,index)=>{
            let createdAt=moment(data.createdAt).year()+"-"+moment(data.createdAt).month()
                +"-"+moment(data.createdAt).day()
            return  <div  key={index}>
                    <Row>
                        <Col md={4}>
                        <img src={data.image === "string" ? defaultImg: data.image} 
                        width="300px" height="auto" alt="defaultImage">
                        </img>
                        </Col>
                        <Col md={8}>
                        <h3>{data.title}</h3>
                        <p className="text-muted">
                            Created Date : {createdAt}
                        </p>
                        <p className="text-muted">Category : {data.category == null ? "No Type" : data.category.name}</p>
                        <p>{data.description}</p>
                        <Link to ={`/View/${data._id}`}>
                            <Button style={{borderBottom:"5px solid mediumspringgreen"}} className="mr-1">VIEW</Button>
                        </Link>
                        <Link to ={`/Update/${data._id}`}>
                        <Button variant="warning" className="mr-1">EDIT</Button>                        
                        </Link>
                        <Button variant="danger" onClick={(id)=> this.handleDelete(data._id)}>DELETE</Button>
                        </Col>
                    </Row>
                    <hr className="mt-3" style={{borderTop:"2px solid lightgrey"}}/>
                    </div>
        })
        console.log(this.props.category)
        return (
            <div className="container">
            <Row className="mb-3">
                <Col >
                <h1>Article Management</h1>
                </Col>
                <Col md={7} className="d-flex align-items-center">
                <span className="mr-1">Category</span>
                <div className="mr-1">
                    <select className="browser-default custom-select">
                    <option>All</option>
                    {this.props.category.map((option)=>{
                        return <option key={option.id} value={option.name}>{option.name}</option>
                    })}
                    </select>
                </div>
                <Form inline>
                    <FormControl type="text" placeholder="Search" className="mr-sm-2" onChange={this.handleSearch.bind(this)}/>
                    <Button style={{borderBottom:"5px solid limegreen"}} variant="outline-success" className="mr-2" >SEARCH</Button>
                    {/* <Button variant="outline-success" onChange={this.handleAdd(this)}>ADD ARTICLE</Button> */}
                    <Link to ={`/Add/`}>
                        <Button variant="primary" className="mr-1">Add Articles</Button>                        
                    </Link>
                </Form>
                </Col>
            </Row>
            <InfiniteScroll
                dataLength={data.length}
                next={this.getMoreArticles.bind(this)}
                hasMore={this.state.hasNext}
                loader={<BoxLoading />}
                endMessage={
                    <p style={{ textAlign: "center" }}>
                    <b>Yay! You have seen it all</b>
                    </p>
                }>
                {data}    
            </InfiniteScroll>
            </div>

        )
    }
}
const mapStateToProps = (state)=>{
    return {data:state.articleReducer.data,
        category:state.articleReducer.cate
    }
}
const mapDispatchToProps = (dispatch)=>{
    return bindActionCreators({
        getArticles,
        getMoreArticles,
        getArticlesById,
        updateArticlesById,
        getCategory,
        searchArticle,
        deleteArticle
        
    },dispatch)
}
export default connect(mapStateToProps,mapDispatchToProps)(HomeBody)