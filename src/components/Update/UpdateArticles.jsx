import React from "react";
import {Form, Button, FormGroup,  } from "react-bootstrap";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { updateArticlesById } from "../../redux/actions/article/articleActions";
import { withRouter } from "react-router-dom";
const defaultImg = "https://increasify.com.au/wp-content/uploads/2016/08/default-image.png"
class UpdateArticles extends React.Component {
  constructor() {
    super();
    this.state = {
        data: [],
            title: "",
            description: "",
        
    };      
} 
  componentDidMount() {
    const { match: { params }} = this.props
    console.log("===========",params.id)
    this.props.updateArticlesById(params.id)
  }
  changeTitleHandler=(e)=>{
    this.setState({title:e.target.value})
} 
changeDesHandler=(e)=>{
   this.setState({title:e.target.value})
} 
  render() {
    console.log("Hi Update");
    
    console.log(this.props.article);
      return (      
        <div className="container">
        <h1>Update Article</h1>
        <div className="row">
        <div className="col-8">
        <Form >
        <Form.Group controlId="formBasicEmail">
          <Form.Label >Title</Form.Label>
          {/* <Form.Control type="text" onChange={this.changeTitleHandler.bind(this)} />  {this.state.article.title} */}

        </Form.Group>   
        <Form.Group>
        <Form.Label style={{marginRight:"10px"}}>Category:</Form.Label>
        <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" id="customRadioInline1" name="customRadioInline1" class="custom-control-input"/>
        <label class="custom-control-label" for="customRadioInline1">News</label>
      </div>
      <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" id="customRadioInline2" name="customRadioInline1" class="custom-control-input"/>
        <label class="custom-control-label" for="customRadioInline2">Sport</label>
      </div>
      </Form.Group> 
        <Form.Group controlId="formBasicPassword">
          <Form.Label >Description</Form.Label>
          {/* <Form.Control type="text" onChange={this.changeDesHandler.bind(this)} />{this.state.article.description} */}

        </Form.Group>
        <Form.Label >Thumnail</Form.Label>
        <div class="custom-file">
  <input type="file" class="custom-file-input" id="customFile" />
  <label class="custom-file-label" for="customFile" >Custom File Input</label>
</div>
        <Button variant="info" type="submit" style={{ padding:"10px",marginRight:"10px",marginTop:"10px",width:"70px"}} >
          Save
        </Button>
      </Form>
        </div>
        <div className="col-4">
        <Form.Group>
        <img src={"https://media.wired.com/photos/5cdefc28b2569892c06b2ae4/master/w_2560%2Cc_limit/Culture-Grumpy-Cat-487386121-2.jpg"} alt="image" width="100%"/>    
        </Form.Group>
        </div>
    </div>
  </div>
      );
  }

}

const mapStateToProps = (state) => {
    console.log("update",state.articleReducer.article);
    
    return {
      article: state.articleReducer.article,
    };
  };
const mapDispatchToProps = (dispatch)=>{
    return bindActionCreators({
        updateArticlesById,
    },dispatch)
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(UpdateArticles));
