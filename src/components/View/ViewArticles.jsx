import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { getArticlesById } from "../../redux/actions/article/articleActions";
import { withRouter } from "react-router-dom";
const defaultImg = "https://increasify.com.au/wp-content/uploads/2016/08/default-image.png"
class ViewArticles extends React.Component {

  componentDidMount() {
    const { match: { params }} = this.props
    console.log("===========",params.id)
    this.props.getArticlesById(params.id)
    
  }
  
  render() {
    console.log(this.props.article);
    if (this.props.article) {
      return (
          
        <div className="container">
        <div className="row">
        <div className="col-4">
        <img src={this.props.article.image == "string" ? defaultImg: this.props.article.image} 
        width="350px" height="auto" alt="defaultImage">
        </img>
       </div>
       <div className="col-8">
       <h3>{this.props.article.title}</h3>
       <p style={{fontSize:"12px",color:"grey"}}> Create Date: {this.props.article.createdAt.substring(0, 10)}</p>
       <p style={{fontSize:"12px",color:"grey"}}>Category :{this.props.article.category == null ? "No Type" :this.props.article.category.name} </p>
       <p>{this.props.article.description}</p>

       </div>
        </div>
        </div>
      );
    }
    return <div className="container"></div>;
  }

}

const mapStateToProps = (state) => {
    console.log("view",state.articleReducer.article);
    
    return {
      article: state.articleReducer.article,
    };
  };
const mapDispatchToProps = (dispatch)=>{
    return bindActionCreators({
        getArticlesById,
    },dispatch)
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ViewArticles));
