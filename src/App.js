import React, { Component } from 'react'
import Navbar from './components/NavBar'
import { Switch, Route } from 'react-router-dom';
import HomeBody from './components/HomeBody';
import Category from './components/Category'
import {BrowserRouter as Router} from 'react-router-dom'
import ViewArticles from './components/View/ViewArticles';
import UpdateArticles from './components/Update/UpdateArticles';
import AddArticles from './components/Add/AddArticles';
export default class App extends Component {
  render() {
    return (
      <div>
      <Navbar />
      <Switch>
      <Route exact path="/">
        <HomeBody />
      </Route>
      <Route path="/category" exact component={Category}/>
      <Route path="/View/:id">
        <ViewArticles/>
      </Route>
      <Route path='/Update/:id' >
      <UpdateArticles/>
      </Route>
      <Route path='/Add/'exact component={AddArticles} >
      <AddArticles/>
      </Route>
    </Switch>
    </div>
    )
  }
}
