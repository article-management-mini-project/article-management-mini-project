import { GET_ARTICLES ,GET_ARTICLES_BY_ID,PATCH_ARTICLES_BY_ID} from "../actions/article/articleActionTypes";
import { GET_CATEGORY, POST_POSTCATEGORY, DELETE_CATEGORY } from "../actions/article/articleActionTypes";
import { GET_MORE_ARTICLES } from "../actions/article/articleActionTypes";
import {   EDIT_CATEGORY, SEARCH_ARTICLE, DELETE_ARTICLE} from "../actions/article/articleActionTypes";
const defaultState ={
    data:[],
    cate:[],
    delete: [],
    deleteCate: []
}
export const articleReducer = (state=defaultState,action) =>{
    switch(action.type){
        case GET_ARTICLES:
            return {
                ...state,
                data:action.data
            }
        case GET_ARTICLES_BY_ID:
            return {
                ...state,
                article:action.data  
            }       
            
        case GET_CATEGORY:
            return {
                ...state,
                cate:action.data
            }
        case DELETE_CATEGORY:
            return {
                ...state,
                deleteCate:action.data
            }
        case POST_POSTCATEGORY:
        case GET_MORE_ARTICLES:
            return {
                ...state,
                data:action.data
            }
        case EDIT_CATEGORY:
            return {
                ...state,
                data:action.data
            }
        case PATCH_ARTICLES_BY_ID:
            return {
                ...state,
                article:action.data            
            }
        case SEARCH_ARTICLE:
            return{
                ...state,
                data:action.data
        }
        case DELETE_ARTICLE:
            return {
                ...state,
                delete:action.data
            }
        default:
            return state
    }
}

