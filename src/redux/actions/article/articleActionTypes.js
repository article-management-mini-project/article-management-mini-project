export const GET_ARTICLES="GET_ARTICLES"
export const GET_ARTICLES_BY_ID = "GET_ARTICLES_BY_ID"
export const GET_CATEGORY="GET_CATEGORY"
export const POST_POSTCATEGORY="POST_POSTCATEGORY"
export const DELETE_CATEGORY="DELETE_CATEGORY"
export const UPDATE_CATEGORY="UPDATE_CATEGORY"
export const PATCH_ARTICLES_BY_ID = "PATCH_ARTICLES_BY_ID"
export const EDIT_CATEGORY="EDIT_CATEGORY"
export const GET_MORE_ARTICLES="GET_MORE_ARTICLES"
export const SEARCH_ARTICLE="SEARCH_ARTICLE"
export const DELETE_ARTICLE="DELETE_ARTICLE"
export const POST_ARTICLE="POST_ARTICLE"
