const { default: Axios } = require("axios")
const {  UPDATE_CATEGORY,PATCH_ARTICLES_BY_ID,GET_ARTICLES, GET_CATEGORY, POST_POSTCATEGORY, DELETE_CATEGORY, GET_ARTICLES_BY_ID, EDIT_CATEGORY, POST_ARTICLE } = require("./articleActionTypes")
//import {  GET_ARTICLES_BY_ID ,PATCH_ARTICLES_BY_ID} from "./articleActionTypes"
//import { GET_ARTICLES, GET_ARTICLES_BY_ID ,PATCH_ARTICLES_BY_ID} from "./articleActionTypes"
const { GET_MORE_ARTICLES } = require("./articleActionTypes")
const {  SEARCH_ARTICLE, DELETE_ARTICLE } = require("./articleActionTypes")
//import {  GET_ARTICLES_BY_ID ,PATCH_ARTICLES_BY_ID} from "./articleActionTypes"
//import { GET_ARTICLES, GET_ARTICLES_BY_ID ,PATCH_ARTICLES_BY_ID} from "./articleActionTypes"
export const getArticles = () =>{
    const innerGetArticles = async (dispatch) =>{
        const result = await Axios.get("http://110.74.194.125:3535/api/articles?page=1&&size=20")
        dispatch({
            type:GET_ARTICLES,
            data:result.data.data
        })
    }
    return innerGetArticles
}
export const getMoreArticles = (page,callback)=>{
    const innerGetMoreArticles = async (dispatch) =>{
        const result = await Axios.get(`http://110.74.194.125:3535/api/articles?page=1&size=15}`)
        dispatch({
            type:GET_MORE_ARTICLES,
            data:result.data.data
        })
        callback(result.data.data.length)
    }
    return innerGetMoreArticles
}
export const updateArticlesById = (id) =>{
    return async dp => {
        const result = await Axios.patch(`http://110.74.194.125:3535/api/articles/${id}`)
        console.log("====**" , result)
        dp({
            type: PATCH_ARTICLES_BY_ID,
            data: result.data.data
        })
    }
}
export const getArticlesById = (id) => {
    return async dp => {
      const result = await Axios.get(`http://110.74.194.125:3535/api/articles/${id}`)
      console.log("====",result)
      dp({
        type: GET_ARTICLES_BY_ID,
        data: result.data.data
      })
    }
  }
export const getCategory = () =>{
    const innerGetCategory = async (dispatch) =>{
        const category = await Axios.get("http://110.74.194.125:3535/api/category")
        dispatch({
            type:GET_CATEGORY,
            data:category.data.data
        })
    }
    return innerGetCategory
}
export const deleteCategory = (id) =>{
    const innerDeleteCategory = async (dispatch) =>{
        const del = await Axios.delete(`http://110.74.194.125:3535/api/category/${id}`)
        dispatch({
            type:DELETE_CATEGORY,
            data:del.data.data
        })
    }
    return innerDeleteCategory
}
export const postCategory = (name) =>{
    const innerPostCategory = async (dispatch) =>{
        const add = await Axios.post(`http://110.74.194.125:3535/api/category`,{name
    })
        dispatch({
            type:POST_POSTCATEGORY,
            data:add.data.data
        })
    }
    return innerPostCategory
}
export const editCategory = (id,name) =>{
    const innerEditCategory = async (dispatch) =>{
        const edit = await Axios.put(`http://110.74.194.125:3535/api/category/${id}`,{name
    })
        dispatch({
            type:EDIT_CATEGORY,
            data:edit.data.data
        })
    }
    return innerEditCategory
}
export const searchArticle = (title) => {
    const innerSearch=async(dispatch)=>{
        const result=await Axios.get(`http://110.74.194.125:3535/api/articles?title=${title}`)
        dispatch({
            type:SEARCH_ARTICLE,
            data:result.data.data
        })
    }
    return innerSearch
};
export const deleteArticle = (id) => {
    const innerDeleteArticle = async (dispatch) =>{
        const del = await Axios.delete(`http://110.74.194.125:3535/api/articles/${id}`)
        dispatch({
            type:DELETE_ARTICLE,
            data:del.data.data
        })
    }
    return innerDeleteArticle
}
// export const addArticle = () => {
//     const innerPostArticle = async (dispatch) =>{
//         const add = await Axios.delete(`http://110.74.194.125:3535/api/articles`)
//         dispatch({
//             type:POST_ARTICLE,
//             data:add.data.data
//         })
//     }
//     return innerPostArticle
// }
export const postPosts = (file, callBack) => {
    Axios.post(`http://110.74.194.125:3535/api/images`, file).then(res => {
        callBack(res.data.url)
    })
  }
  export const addArticle = (article, callBack) => {
    Axios.post(`http://110.74.194.125:3535/api/articles`, article).then(res => {
        callBack(res.data.message)
    })
  }
